let people = [
    { name: "bob", id: 1 }, { name: "john", id: 2 },
    { name: "alex", id: 3 }, { name: "john", id: 3 }, { name: "alex", id: 3 }
];

let people_name = people.map(person => person.name);

let counter = people_name.reduce((count, item) => {
    count[item] = (count[item] || 0) +1;
    return count;
}, {})
console.log(counter);