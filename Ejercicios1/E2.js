//Ejercicio 1
console.log('-----Ejercicio 1-------');
function f(x, y = 2, z = 7) {
    return x + y + z;
}
console.log(f(15, undefined));


console.log('-----Ejercicio 2-------');
var animal = 'kitty';
var result = (animal === 'kitty') ? 'cute' : 'still nice';

console.log(result)

console.log('-----Ejercicio 3-------');
//ejercicio 2
function res(animal) {
    var animal = 'kittyy';
    var result = '';
    if (animal === 'kitty') {
        return result = 'cute'
    } else {
        return result = 'still nice';

    }
}
console.log(res(animal))

console.log('-----Ejercicio 4-------');
function f() {
    var a = 0;
    var str = 'not a';
    var b = '';
    b = a === 0 ? (a = 1, str += ' test') : (a = 2);
    return console.log(b);
}
f()

console.log('-----Ejercicio 5-------');
function f() {
    var a = 1;
    a === 1 ? console.log('Hey, it is 1!') : 0;
    return console.log(a);
}
console.log(f());
console.log('-----Ejercicio 6-------');
var a = 0;
function f() {
    a === 1 ? console.log('Hey, it is 1!') :
        console.log('weird, what could it be');
    if (a === 1) console.log('Hey, it is 1!')
    else console.log('weird, what could it be?');
    return console.log(a);
}
console.log(f());

console.log('-----Ejercicio 7-------');

function cont() {
    var animal = 'kitty';
    for (var i = 0; i < 5; ++i) {
        (animal === 'kitty');
        console.log(i);
    }

}
cont()

console.log('-----Ejercicio 8-------');
function sw() {
    var value = 1;
    switch (value) {
        case 1:
            return value = console.log('I will always run');
            break;
        case 2:
            return value = console.log('I will never run')
            break;

    }
}
console.log(sw());


console.log('-----Ejercicio 9-------');

function an() {
    var animal = 'lion';
    switch (animal) {
        case 'Dog':
            console.log('I Will not run since animal !== "Dog');
            break;
        case 'Cat':
            console.log('I Will not run since animal !== "Cat');
            break;

        default:
            console.log('I will run since animal does not march any onther case');
    }
}
console.log(an())

console.log('-----Ejercicio 10-------');
name = jhon()
function jhon() {
    return 'Jhon';
}

function jacob() {
    return 'Jacob';
}

function cs() {
    switch (name) {
        case jhon():
            console.log('I will run if name === "Jhon"');
            break;

        case 'Ja' + 'ne':
            console.log('i Wll run if name === "Jane"');
            break;

        case jhon() + ' ' + jacob() + 'Jingleheimer Schmidt': console.log('his name is equal to name too')
            break;
    }
}

console.log(cs());

console.log('-----Ejercicio 11-------');

function cs1() {
    var x = 'c';
    switch (x) {
        case 'a':
        case 'b':
        case 'c':
            console.log('Either a, b or c was selected');
            break;
        case 'd':
            console, log('Only d was selected');
            break;
        default:
            console, log('Only d was selected');
            break;
    }
}

console.log(cs1())

console.log('-----Ejercicio 12-------');

var a = 5 + 7;
var b = 5 + "7";
var x = "5" + 7;
var c = 5 - 7;
var d = 5 - "7";
var e = "5" - 7;
var f = 5 - "x";

console.log(x);
console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);
console.log(f);

console.log('-----Ejercicio 13-------');
var a = 'hello' || '';
var b = '' || [];
var c = '' || undefined;
var d = '' || 5;
var e = 1 || {};
var f = 0 || '' || 5;
var g = '' || 'yay' || 'boo';
function num(a, b, c, d, e, f, g) {

    switch (arguments.length) {

        case "1":
            a = 'hello' || '';

        case "2":
            b = '' || [];


        case "3":
            c = '' || undefined;

        case "4":
            d = '' || 5;

        case "5":
            e = 1 || {};

        case "6":
            f = 0 || '' || 5;

        case "7":
            g = '' || 'yay' || 'boo';

    }
    return [a, b, c, d, e, f, g];
}

console.log(num(a, b, c, d, e, f, g));


console.log('-----Ejercicio 14-------');
//Ejercicio 13
var a = 'hello' && '';;
var b = '' && [];
var c = undefined && 0;
var d = 1 && 5;
var e = 0 && {};
var f = 'hi' && [] && 'done';
var g = 'bye' && undefined && 'adios';
function num(a, b, c, d, e, f, g) {

    switch (arguments.length) {

        case "1":
            a = 'hello' && '';

        case "2":
            b = '' && [];


        case "3":
            c = undefined && 0;

        case "4":
            d = 1 && 5;

        case "5":
            e = 0 && {};

        case "6":
            f = 'hi' && [] && 'done';

        case "7":
            g = 'bye' && undefined && 'adios';

    }
    return [a, b, c, d, e, f, g];
}

console.log(num(a, b, c, d, e, f, g));

console.log('-----Ejercicio 15-------');


console.log('-----Ejercicio 16-------');
var foo = function (val) {
    return val || 'default';
}
console.log(foo('burge'));
console.log(foo(100));
console.log(foo([]));
console.log(foo(0));
console.log(foo(undefined));

console.log('-----Ejercicio 17-------');
var status = 'royal';
var Height = 4;
var age = 2;
var isLegal = age >= 18;
var tall = Height >= 5.11;
var suitable = isLegal && tall;
var isRoyalty = status === 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterOurBar = suitable || specialCase;



console.log('-----Ejercicio 18-------');
for (var i = 0; i < 3; i++) {
    if (i === 1) {
        continue;
    }
    console.log(i);
}

console.log('-----Ejercicio 19-------');
var i = 0;
function i() {
    while (i < 3) {
        if (i === 1) {
            i = 2;
            continue;
        }
        console.log(i);
        i++;
    }
}

console.log('-----Ejercicio 20-------');
console.log('-----Ejercicio 21-------');
var namedSum = function sum (a,b){
    return a + b;
}
var anonSum = function (a,b) {
    return a + b;
}
console.log (namedSum(1,3));
console.log (anonSum(1,3));
console.log('-----Ejercicio 22-------');
function foo(){
    const a = true;
    function bar(){
        const a = false;
        console.log (a);
    }
    const b = false;
    b = false;
    console.log(b);
}

