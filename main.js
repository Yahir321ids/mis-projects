class MiMensaje extends HTMLElement {
    constructor() {
        super();
        this.addEventListener('click', (e) => {
            alert('click mensaje');
        });
        console.log('Elemento creado en el constructor');
    }

    /*propiedad solo para escuchar los atributos definidos en el arreglo
    usando attribueChangedCallback*/
    static get observedAttributes() {
        return ['msj'];
    }

    //scallback se inserta en el elemento dom
    connectedCallback() {
        console.log('connectedCallback: Cuando el elemento es insertado en el documento');
    }

    disconnectedCallback() {
        alert('Disconnected: Cuando el elemento es eliminado del documento');
    }

    adoptedCallback(){
        alert('Adoptedcallback: Cuando el elemento es adoptado por otro documento')
    }

    //Cunado un atributo es modificado, solo llamado en atributos observados en 
    //la propiedad observedAttributes
    attributeChangedCallback (atterNaame, oldVal, newVal){
        console.log('attributeChangedCallback: Cuando cambia un atributo');
        if (atterNaame === 'msj'){
            this.pintarMensaje(newVal);
        }
    }

    pintarMensaje (msj){
        this.innerHTML = msj
    }

    get msj(){
        return this.getAttribute('msj');
    }

    set msj(val){
        this.setAttribute('msj', val)
    }
}

customElements.define('mi-mensaje', MiMensaje);


//Segundo mensaje
let miMensaje = document.createElement('mi-mensaje');
miMensaje.msj = 'otro mensaje';
document.body.appendChild(miMensaje);

//Tambien puedes crear un elemento con el operador new
//tercer mensaje
let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);